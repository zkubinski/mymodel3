#include "mymodel.h"
#include <QString>
#include <QDebug>

MyModel::MyModel(QObject *parent) : QAbstractTableModel{parent}
{
    for(int i=0; i<8; ++i){
        tablica.push_back(i*458);
    }
}

int MyModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return tablica.count();
}

int MyModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 2;
}

QVariant MyModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid()==true){
        return QVariant();
    }

    if(role == Qt::DisplayRole){
        if(index.column()==0)
        return tablica.at(index.row());
    }

    return QVariant();
}

bool MyModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(!index.isValid()==true){
        return false;
    }

    if(role==Qt::EditRole){
        bool ok = false;
        int x = value.toInt(&ok);
        if(ok){
            tablica[index.row()]=x;
            emit dataChanged(index,index,QVector<int>{Qt::EditRole});
            return true;
        }
    }

    return false;
}

QVariant MyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role==Qt::DisplayRole && orientation==Qt::Horizontal){
        if(section==0){
            QVariant header0;
            header0="Dane";
            return header0;
        }
        if(section==1){
            QVariant header1;
            header1="Brak danych";
            return header1;
        }
    }
    if(role==Qt::DisplayRole && orientation==Qt::Vertical){
        return QString ("%1").arg(section+1);
    }
    return QVariant();
}

Qt::ItemFlags MyModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flag = QAbstractItemModel::flags(index);

    if(index.column()==0 || index.column()==1){
        flag |= Qt::ItemIsEditable;
        return flag;
    }

    return flag;
}

