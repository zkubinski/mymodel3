#ifndef DELEGATEFORTABLEVIEW_H
#define DELEGATEFORTABLEVIEW_H

#include <QItemDelegate>
#include <QLineEdit>

class DelegateForTableView : public QItemDelegate
{
    Q_OBJECT

public:
    explicit DelegateForTableView(QObject *parent = nullptr);
    ~DelegateForTableView();

    // QAbstractItemDelegate interface
private:
    virtual QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const override;
    virtual void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const override;
    virtual void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

#endif // DELEGATEFORTABLEVIEW_H
