#include "delegatefortableview.h"

DelegateForTableView::DelegateForTableView(QObject *parent) : QItemDelegate{parent}
{

}

QWidget *DelegateForTableView::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    QLineEdit *editField = new QLineEdit(parent);

    return editField;
}

void DelegateForTableView::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QString text = index.model()->data(index, Qt::EditRole).toString();

    QLineEdit *editField = static_cast <QLineEdit*>(editor);
    editField->setText(text);
}

void DelegateForTableView::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QLineEdit *editField = static_cast<QLineEdit*>(editor);
    QString text = editField->text();
    model->setData(index, text, Qt::EditRole);
}

void DelegateForTableView::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    editor->setGeometry(option.rect);
    Q_UNUSED(index);
}

DelegateForTableView::~DelegateForTableView()
{
}
