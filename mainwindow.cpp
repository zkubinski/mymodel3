#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    this->resize(500,400);

    mainWidget = new QWidget(this);
    mainLayout = new QVBoxLayout(mainWidget);

    lineEdit = new QLineEdit(this);
    tableView = new QTableView(this);
    delegate = new DelegateForTableView(this);

    myModel = new MyModel();
    completer = new QCompleter(this);

    tableView->setModel(myModel);
    tableView->setItemDelegate(delegate);

    completer->setModel(myModel);
    lineEdit->setCompleter(completer);

    QObject::connect(lineEdit, &QLineEdit::textChanged, this, &MainWindow::matchWord);

    mainLayout->addWidget(lineEdit);
    mainLayout->addWidget(tableView);

    setCentralWidget(mainWidget);
}

void MainWindow::matchWord(const QString &matchStr)
{
    match = myModel->match(myModel->index(0,0), Qt::DisplayRole, matchStr);

    foreach(QModelIndex idx, match){
        tableView->setCurrentIndex(myModel->index(idx.row(),0));
    }
}

MainWindow::~MainWindow()
{
}

