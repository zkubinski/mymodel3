#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QTableView>
#include <QCompleter>
#include <QModelIndexList>

#include "mymodel.h"
#include "delegatefortableview.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QWidget *mainWidget;
    QVBoxLayout *mainLayout;
    QLineEdit *lineEdit;
    QTableView *tableView;

    MyModel *myModel;
    QCompleter *completer;
    QModelIndexList match;
    DelegateForTableView *delegate;

public slots:
    void matchWord(const QString &matchStr);
};
#endif // MAINWINDOW_H
